if pgrep -x "node" > /dev/null
then
    echo "Forever still running..."
else
    echo "Detected forever stopped... Restarting..."
    sudo runuser -l admin -c "forever stopall"
    sudo runuser -l admin -c "cd /home/admin/tribot-helper/manager && forever start script_manager.js"
fi