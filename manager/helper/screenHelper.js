import fs from 'fs';
import { execSync } from 'child_process';

export const fixScreenSize = () => {

	try {

		const configPath = `/home/admin/.tribot/settings/settings.json`;

		const configData = fs.readFileSync(configPath, {
			encoding: 'utf-8',
			flag: 'r'
		})

		const configJson = JSON.parse(configData);

		if (configJson.splitPanePosition === 531) {

			console.log('Debug overlay is active, attempting to collapse it...');

			execSync('xdotool mousemove 18 590 click 1');

			execSync('xdotool mousemove 3 590 click 1');

		} else {

			return;
		}

	} catch (error) {

		console.error({
			error: error.message
		})

	}
}