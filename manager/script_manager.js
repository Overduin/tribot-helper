
import * as dotenv from 'dotenv';
import { io } from 'socket.io-client';
import { spawn, exec, execSync } from 'child_process';
import fs from 'fs';
import fetch from 'node-fetch';
import FormData from 'form-data';
import { stopXvfb } from './helper/commandHelper.js';
import { getMuleIp, getTribotUserInfo, getUserInfo } from './helper/credentialsHelper.js';
import { fixScreenSize } from './helper/screenHelper.js';


dotenv.config();

const forwardPortTrafficToMule = () => {

	const { ip, port } = getMuleIp();

	console.log('Forwarding...:', ip, port);

	const socatProcess = exec(`sudo socat TCP-LISTEN:${port},fork,bind=localhost TCP:${ip}:${port}`);

	socatProcess.stdout.on('data', (data) => {

		console.log('Socat output: ', data.toString());
	})

	socatProcess.stderr.on('data', (data) => {

		console.log('Socat error: ', data.toString());
	})

	socatProcess.on('exit', (code) => {

		console.log('Socat exited with code: ' + code);
	})
}

const socket = io(process.env.API_URL, {
	reconnection: true,
})

socket.on('connect', () => {

	console.log('socketId', socket.id)

	console.log('connected')
})

socket.on('disconnect', () => {

	console.log('disconnect', socket.id);
})

socket.on('error', e => {

	console.log('error', e)
})

socket.on('*', e => {

	console.log('event received', e);
})

socket.on('stop_script', () => {
	/** Clicks 'Stop script' for a safe shutdown */
	execSync('xdotool mousemove 650 45 click 1');
})

socket.on('kill_script', () => {
	/** Kills Xvfb */
	stopXvfb();
})

socket.on('restart_script', () => {

	execSync('xdotool mousemove 735 45 click 1');
})

socket.on('show_logs', () => {

	showBotDebug();
})

socket.on('start_script', (event) => {

	const { scriptName, scriptArgs } = JSON.parse(event);

	if (!`${scriptName}`.includes('Mule')) {

		forwardPortTrafficToMule();
	}

	console.log('Running script', scriptName, scriptArgs);

	runScript(scriptName, scriptArgs);
})


const runScript = async (scriptName, scriptArgs) => {

	stopXvfb();

	const { tribotUsername, tribotPassword } = getTribotUserInfo();

	const { username, password } = getUserInfo();

	const resolutionWidth = 760;
	const resolutionHeight = 690;
	const colors = 24;

	const fullCommand = [
		'xvfb-run',
		'-s',
		`'-screen 0 ${resolutionWidth}x${resolutionHeight}x${colors}'`,
		'/home/admin/.tribot/install/Splash/jre/bin/java',
		'-Xms256M',
		'-Xmx512M',
		'-jar',
		'/home/admin/.tribot/install/Splash/tribot-splash.jar',
		'--fulldebug',
		'--username',
		tribotUsername,
		'--password',
		tribotPassword,
		'--charusername',
		username,
		'--charpassword',
		password,
		'--script',
		scriptName,
		'--scriptargs',
		scriptArgs,
		'--breakprofile',
		'Standard_break',
	].join(' ')

	console.log('Running commmand', fullCommand)

	socket.emit('script_started', { command: fullCommand });

	const childProcess = exec(fullCommand);

	childProcess.stdout.on('data', (data) => {

		handleScriptOutput(data.toString());
	})

	childProcess.stderr.on('data', (data) => {

		console.error(data.toString());
	})

	childProcess.on('exit', (code) => {

		console.log('child process exited with code: ' + code.toString())
	})
}

socket.on('update_script', () => {

execSync(`cd /home/admin/tribot-helper && git pull`);

	execSync('sudo cp /home/admin/tribot-helper/script_config/* /home/admin/.tribot/');

	execSync('sudo chown admin:admin /home/admin/*');

	execSync('forever restartall');
})

const getScreenshot = () => {

	const filename = `${process.cwd()}/screenshots/screenshot_${new Date().getTime()}.png`;

	const command = `DISPLAY=${process.env.DISPLAY} import -window root ${filename}`;

	try {

		execSync(command);

		return filename;

	} catch (error) {

		// console.log({ error: error.message })

		const authCommands = execSync('ps aux | grep auth');

		const authCommandList = authCommands.toString().split('\n')

		const authStats = authCommandList[0].split(' ')

		const authPath = authStats[authStats.length - 1];

		if (authPath.includes('xvfb')) {
			// Copy the authority file for xvfb to the logged in user
			execSync(`cp ${authPath} /home/admin/.Xauthority`)
		}

		// console.log({ authPath, socketId: socket.id })

		return null;
	}
}

const postDataToApi = async (filename) => {

	const { username } = getUserInfo();

	const formData = new FormData();

	formData.append('file', fs.createReadStream(filename, {
		contentType: 'image/png',
		filename,
	}));

	formData.append('username', username)

	await fetch(`${process.env.API_URL}/screen-data`, {
		method: 'POST',
		body: formData,
	});

	// console.log({ filename })

	execSync('rm /home/admin/tribot-helper/manager/screenshots/screenshot_*')
}

const handleScriptEvents = async () => {

	// fixScreenSize();

	// fixResizeable();

	const filename = getScreenshot();

	if (filename) {

		await postDataToApi(filename);
	}
}

const loop = async () => {

	try {

		await handleScriptEvents();

	} catch (error) {

		console.warn({ error: error.message })
	}

	const timeout = setTimeout(() => {

		clearTimeout(timeout);

		process.nextTick(loop);

	}, 5000)
}

try {

	loop();

} catch (error) {

	console.log({
		loopError: error
	})
}

const fixResizeable = () => {

	console.log('Fixing resizeable by clicking settings...')

	execSync('xdotool mousemove 660 575 click 1')

	execSync('xdotool mousemove 640 460 click 1')

	execSync('xdotool mousemove 640 495 click 1');

	console.log('Does it work?...')
}

const showBotDebug = () => {

	console.log('Showing logs')

	execSync('xdotool mousemove 30 615 click 1')
}


console.log('Script manager running...');

let resizeableFixed = false;

const handleScriptOutput = (logText) => {

	if (logText.includes('[Object Handler] Sort Order: Crevice [Jump-Down]')) {

		if (!resizeableFixed) {

			resizeableFixed = true;

			fixResizeable();
		}
	}

	console.log(logText);
}

const runRevenantScript = (scriptArgs) => {

	runScript('Revenant', scriptArgs);
}

const runAIOScript = (scriptArgs) => {

	runScript('\"AIO Account Builder\"', scriptArgs)
}

const runMuleScript = () => {

	runScript('G.Mule', 'Main');
}

// if (Boolean(Number(process.env.IS_MULE))) {

// 	runMuleScript();

// } else {

// 	forwardPortTrafficToMule();

// 	runRevenantScript('Main_avarice_70+_range');
// }