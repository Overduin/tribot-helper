#!/bin/sh

# Full install script for Tribot Revs
# Args are: USERNAME, PASSWORD, TRIBOT_USERNAME, TRIBOT_PASSWORD

# Unset display value
unset DISPLAY
sudo apt-get update
# Download the installer
wget https://installers.tribot.org/TribotInstaller.sh
# Run the installer and press enter three times for default settings
echo -ne "\n\n\n" | bash TribotInstaller.sh
# Install required packages
sudo apt-get install nodejs npm xvfb libxrender1 libxtst6 ffmpeg libgtk-3-0 imagemagick xdotool -y
# Install forever
sudo npm i -g forever
# Clone repo
git clone https://gitlab.com/Overduin/tribot-helper.git
# Copy .env file
sudo cp /home/admin/tribot-helper/manager/.env.production /home/admin/tribot-helper/manager/.env
# Store credentials
sudo bash -c "echo \"$USERNAME:$PASSWORD\" > /home/admin/credentials"
sudo bash -c "echo \"$TRIBOT_USERNAME:$TRIBOT_PASSWORD\" > /home/admin/tribot_credentials"
sudo bash -c "echo \"$MULE_PRIVATE_IP:$MULE_PORT\" > /home/admin/mule_ip"
# Move configs
sudo cp /home/admin/tribot-helper/config/* /home/admin/.tribot/
# Move health check script to user folder
sudo cp /home/admin/tribot-helper/health_check.sh /home/admin/
# Create cron for starting on boot
echo -e "@reboot sudo runuser -l admin -c \x22cd /home/admin/tribot-helper/manager/ && forever start script_manager.js\x22" >> bot_manager_health_check
echo -e "*/1 * * * * . /home/admin/health_check.sh" >> bot_manager_health_check
crontab bot_manager_health_check
rm bot_manager_health_check
# Install node packages
cd /home/admin/tribot-helper/manager && npm i
# Make admin owner of all files
sudo chown admin:admin /home/admin/*
# Start forever
cd /home/admin/tribot-helper/manager/
forever start --uuid bot-manager script_manager.js
# xvfb-run -s '-screen 0 760x690x24' /home/admin/.tribot/install/Splash/jre/bin/java -jar /home/admin/.tribot/install/Splash/tribot-splash.jar  --fulldebug --username "bernardzwijn0@gmail.com" --password "Kankerlul13371!" --charusername "Ashleyz44008@gmail.com" --charpassword "iop4mx50hx7" --script "Revenant" --scriptargs Main