import fs from 'fs';
import * as dotenv from 'dotenv';

dotenv.config();

export const getUserInfo = () => {

	const accountCredentialsPath = process.env.CREDENTIALS_PATH;

	const accountCredentials = fs.readFileSync(accountCredentialsPath, {
		encoding: 'utf-8',
		flag: 'r'
	});

	const [username, password] = accountCredentials.replace('\n', '').split(':');

	return { username, password };
}

export const getTribotUserInfo = () => {

	const tribotCredentialsPath = process.env.TRIBOT_CREDENTIALS_PATH;

	const tribotCredentials = fs.readFileSync(tribotCredentialsPath, {
		encoding: 'utf-8',
		flag: 'r'
	});

	const [tribotUsername, tribotPassword] = tribotCredentials.replace('\n', '').split(':');

	return { tribotUsername, tribotPassword };
}

export const getMuleIp = () => {

	const muleIpPath = process.env.MULE_IP_PATH;

	const muleIpData = fs.readFileSync(muleIpPath, {
		encoding: 'utf-8',
		flag: 'r'
	});

	const [ip, port] = muleIpData.replace('\n', '').split(':');

	return {
		ip,
		port
	}
}