import { exec, execSync } from 'child_process';

export const stopXvfb = () => {

	exec('killall Xvfb');
}

export const stopScripts = (match = '') => {

	return new Promise((resolve, reject) => {

		exec(`ps -A | grep "${match}"`, (error, stdout, stderr) => {

			stdout.split('\n').forEach(line => {

				console.log({ line })

				const [pid] = line.split(' ').filter(Boolean);

				if (Boolean(pid)) {

					console.log('killing pid:', pid)

					exec(`sudo kill -9 ${pid}`);
				}
			})

			resolve();
		})
	})
}